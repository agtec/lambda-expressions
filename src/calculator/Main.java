package calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //method 1
        Calculator calculator = (int a, int b) -> {
            System.out.println("Choose operation: +, -, *, /");
            String operation = sc.nextLine();
            System.out.print(a + " " + operation + " " + b + " = ");
            int result;
            switch (operation) {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
                default:
                    throw new RuntimeException("Unknown operation");
                    //return 0;

            }
            return result;
        };

        try {
            System.out.println(calculator.calculate(10, 20));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }


        //method 2
        int result2 = ((Calculator) (int a, int b) -> {
            System.out.println("Choose operation: +, -, *, /");
            String operation = sc.nextLine();
            System.out.print(a + " " + operation + " " + b + " = ");
            int result;
            switch (operation) {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
                default:
                    throw new RuntimeException("Unknown operation");
            }
            return result;
        }).calculate(8, 0);

        try {
            System.out.println(result2);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

}