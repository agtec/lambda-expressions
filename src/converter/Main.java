package converter;

public class Main {

    public static void main(String[] args) {

        // method 1 - concrete class
        PLNConverter converter1 = new PLNConverter();
        converter1.convert("12");

        Converter converter2 = new Converter() {
            @Override
            public void convert(String numberStr) {
                int number = Integer.parseInt(numberStr);
                System.out.println(number * 4);
            }
        };
        converter2.convert("123");

        // method 2 - anonymuos class
        ((Converter) (String numberStr) -> {
            int number = Integer.parseInt(numberStr);
            System.out.println(number * 4);
        }).convert("1234");

        // method 3 - anonymous class
        Converter converter3 = (String numberStr) -> {
            int number = Integer.parseInt(numberStr);
            System.out.println(number * 4);
        };

        converter3.convert("123456");
    }
}
