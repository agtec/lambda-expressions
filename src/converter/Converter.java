package converter;

public interface Converter {

    void convert(String number);
}
