package toUpperCase;

@FunctionalInterface
public interface ToUpperCase {

    void invoke(String text);
}
