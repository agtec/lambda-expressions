package toUpperCase;

public class Main {

    public static void main(String[] args) {

        System.out.println("Method 1 - concrete class");

        ToUpperCase toUpperCase = new ToUpperCaseImpl();
        String sposob1 = "Java is a general-purpose programming language that is class-based, object-oriented";
        toUpperCase.invoke(sposob1);

        System.out.println("Method 2 - anonymuos class");

        ToUpperCase toUpperCase1 = new ToUpperCase() {
            @Override
            public void invoke(String text) {
                String upperCaseText = text.toUpperCase();
                System.out.println(upperCaseText);
            }
        };

        toUpperCase1.invoke("It is intended to let application developers write once, run anywhere (WORA)");


        System.out.println("Method 2 - anonymuos class - lambda expression");

        ToUpperCase toUpperCase2 = (String napis) -> {
            String upperCaseText = napis.toUpperCase();
            System.out.println(upperCaseText);
        };

        toUpperCase2.invoke("Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture");


        System.out.println("Method 3 - anonymuos class");

        (new ToUpperCase() {
            @Override
            public void invoke(String text) {
                String upperCaseText = text.toUpperCase();
                System.out.println(upperCaseText);
            }
        }).invoke("The syntax of Java is similar to C and C++, but it has fewer low-level facilities than either of them)");


        System.out.println("Method 3 - anonymuos class - lambda expression");

        show((String text1) -> {
            System.out.println(text1.toUpperCase());
        }, "https://en.wikipedia.org/wiki/Java_(programming_language)");
    }

    static void show(ToUpperCase toUpperCase, String text) {
        toUpperCase.invoke(text);

    }
}
