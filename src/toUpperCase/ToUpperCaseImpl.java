package toUpperCase;

public class ToUpperCaseImpl implements ToUpperCase {

    @Override
    public void invoke(String text) {
        String upperCaseText = text.toUpperCase();
        System.out.println(upperCaseText);
    }

}
